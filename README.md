# Luxómetro CiAlGa



## Trabajo práctico de Electrónica Aplicada II y Teoría de Circuitos II. UTN FR San Francisco

En este repositorio se encuentran subidos todos los archivos relacionados al trabajo práctico de amplificadores operacionales de Electrónoca Aplicada II y filtros digitales de Teoría de Circuitos II. Contiene archivos de programación de los microcontroladores, simulaciones en Multisim y programas en MATLAB.

## Autores

- [ ] Cignetti, Mateo
- [ ] Galliano, Ignacio
- [ ] Alesandria, Alejo
