#include <LiquidCrystal.h>

#define SAMPLNG_PERIOD_MS 5
#define RESISTOR 6600

const char pinRs = 23;
const char pinRw = 22;
const char pinE = 1;
const char pinD0 = 3;
const char pinD1 = 21;
const char pinD2 = 19;
const char pinD3 = 18;
const char pinADCInput = 39;

const float inputCoefficients[7] = {0.01165, 0.06992, 0.1748, 0.2331, 0.1748, 0.06992, 0.01165};
const float outputCoefficients[7] = {1, 1.063, -1.201, 0.5836, -0.2318, 0.04371, -0.004295};

float outputArray[7] = {0, 0, 0, 0, 0, 0, 0};
float inputArray[7] = {0, 0, 0, 0, 0, 0, 0}; 

LiquidCrystal lcd(pinRs, pinRw, pinE, pinD0, pinD1, pinD2, pinD3);

unsigned long lastMeasurementTime = 0;

void calculateLux(float ADCValue){
  float ADCVoltageMv = ADCValue * (3300.0/4095.0);
  float LDRVoltageMv = 3300.0 - ADCVoltageMv;
  float LDRResistance = LDRVoltageMv / ADCVoltageMv * RESISTOR;
  
  float LDRLux = 0.89* 2325065959.0 * pow(LDRResistance, -1.837493532);

  lcd.setCursor(0,0);
  lcd.print((int) LDRLux);
  lcd.print(" lux   ");
}

void getInputData(){
  lastMeasurementTime = millis();
  inputArray[0] = float(analogRead(pinADCInput));
}

void iirLowpassButterworthFilter(){
  float outputSum = inputCoefficients[0]*inputArray[0];
  for(int i = 1; i < 7; i++){
    outputSum += inputCoefficients[i] * inputArray[i] + outputCoefficients[i] * outputArray[i];
  }
  outputArray[0] = outputSum;
}

void arraysRightShift(){
  for(int i = 6; i > 0; i--){
    outputArray[i] = outputArray[i-1];
    inputArray[i] = inputArray[i-1];
  }
}

void pinInitalization(){
  pinMode(pinADCInput, INPUT);
  pinMode(pinDACOutput, OUTPUT); 
  pinMode(pinRs, OUTPUT);
  pinMode(pinRw, OUTPUT);
  pinMode(pinE, OUTPUT);
  pinMode(pinD0, OUTPUT);
  pinMode(pinD1, OUTPUT);
  pinMode(pinD2, OUTPUT);
  pinMode(pinD3, OUTPUT);
}

void lcdInitialization(){
  lcd.begin(16, 2);
  lcd.setCursor(0, 1);
  lcd.print("Luxometro CiAlGa");
}

void setup() {
  pinInitalization();
  lcdInitialization();

  analogReadResolution(12);
  lastMeasurementTime = millis();
}

void loop() {
  if(millis() - lastMeasurementTime == SAMPLNG_PERIOD_MS){
    getInputData();
    iirLowpassButterworthFilter();
    calculateLux(outputArray[0]);
    arraysRightShift(); 
  }
}
