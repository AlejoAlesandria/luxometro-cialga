#define SAMPLNG_PERIOD_MS 5

const char pinADCInput = 39;
const char pinDACOutput = 25;

const float inputCoefficients[7] = {0.01165, 0.06992, 0.1748, 0.2331, 0.1748, 0.06992, 0.01165};
const float outputCoefficients[7] = {1, 1.063, -1.201, 0.5836, -0.2318, 0.04371, -0.004295};

float outputArray[7] = {0, 0, 0, 0, 0, 0, 0};
float inputArray[7] = {0, 0, 0, 0, 0, 0, 0}; 

unsigned long lastMeasurementTime = 0;

void getInputData(){
  lastMeasurementTime = millis();
  inputArray[0] = float(analogRead(pinADCInput));
}

void iirLowpassButterworthFilter(){
  float outputSum = inputCoefficients[0]*inputArray[0];
  for(int i = 1; i < 7; i++){
    outputSum += inputCoefficients[i] * inputArray[i] + outputCoefficients[i] * outputArray[i];
  }
  outputArray[0] = outputSum;
}

void arraysRightShift(){
  for(int i = 6; i > 0; i--){
    outputArray[i] = outputArray[i-1];
    inputArray[i] = inputArray[i-1];
  }
}

void pinInitalization(){
  pinMode(pinADCInput, INPUT);
  pinMode(pinDACOutput, OUTPUT); 
}

void setup() {
  pinInitalization();

  analogReadResolution(8);
  lastMeasurementTime = millis();
}

void loop() {
  if(millis() - lastMeasurementTime == SAMPLNG_PERIOD_MS){
    getInputData();
    iirLowpassButterworthFilter();
    dacWrite(pinDACOutput, outputArray[0]);
    arraysRightShift(); 
  }
}
